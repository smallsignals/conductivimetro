#include "stm32f1xx_hal.h"



#define TimeCheck  350
#define TimeReset  2000
#define TimeMode  2000
#define TimeFRM  2000
#define TimeConect  2000
#define TimeIp  2000
#define TimeDconect  2000
#define TimeStatus  2000
#define TimeMux  2000
#define TimeSetConect  1000
#define TimeSend  1500


#define StatusGotIp  2
#define StatusConected  3
#define StatusDisconected  4




#define NTP_PACKET_SIZE  48// NTP time is in the first 48 bytes of message






void InitWifi(void);
void DeInitWifi(void);
void SleepWifi(void);
void RestartWifi(void);

int FindRep(uint8_t *RxBuff,int lenRx,uint8_t *Reply,int lenRep);
_Bool SendComand(uint8_t TxBuff[],int lenTx, int lenRx,int timeRec);

_Bool check(void);
_Bool Reset(void);
void RstBuffer(void);
_Bool CWMode(int mode);
_Bool GetFrm (void);
_Bool Conect(uint8_t *access_point_name,int lenNam, uint8_t *password, int lenPas);
_Bool GetIp (void);
_Bool DesConect(void);
int GetStatus(void);
_Bool SetMux(int Mux);
_Bool SetConnect(char * ip);
_Bool CloseConnect(void);
_Bool SetConnect2(void);
_Bool SendToServer(uint8_t msg[],int lenn);
_Bool SendToServerMult(uint8_t id , uint8_t msg[], int lenn);
_Bool SetServer(uint8_t mode, uint32_t port);
uint32_t GetTime(void);
void sendNTPpacket(void);
void ledWifiFault(void);
