/**
  ******************************************************************************
  * File Name          : main.c
  * Date               : 05/03/2015 11:52:13
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2015 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include <string.h>
#include "wifi.h"
#include "sntp.h"
#include "printf.h"
#include "webserv/servreq.h"
#include "webserv/resource.h"

char device[]="defaultDevice@sergiosanes.sergiosanes";//"cond_jorge@jartieda.jartieda";
char apikey[]="71873932c0b1b029b39d223b7b60fd0ef1a112335432dcabc35ce1d950bccf76";//"ec4fa0c6b83c3682ad8a6c5fcdbfce919f7715bd70410c8ab24a683beb96b230";

uint8_t i2cbuf[224];			//name 64 + ssid 32 + device 64 + apikey 64

//uint8_t TxBuffer[50];
uint8_t RxBuffer[1024];
int exito[35];
int cexito=0;
int RxComp = 0;
int conduct=0;
int temperature=0;
int luz=0;
int valor=0;
int crashnum =0;
int waitLed = 0;

int try =0;
uint32_t TimeCtrl = 0 ;
__IO uint16_t valores[30];
struct wlan{
	uint8_t ssid[64];
	uint16_t ssid_len;
	uint8_t pass[32];
	uint16_t pass_len;
	uint8_t device[64];
	uint16_t device_len;
	uint8_t apikey[64];
}mywlan;

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;
I2C_HandleTypeDef hi2c1;
UART_HandleTypeDef huart1;
IWDG_HandleTypeDef hiwdg;

RTC_HandleTypeDef RtcHandle;
RTC_AlarmTypeDef sAlarm = {{0},0};
RTC_TimeTypeDef sTime = {0};
RTC_DateTypeDef DateToUpdate = {0};


/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_DMA_Init(void);
static void MX_I2C1_Init(void);
static void MX_IWDG_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_RTC_Init(void);
static void RTC_AlarmConfig(void);

void ledInicio(void);
int GetTemp(void);
int GetConduc(void);
int GetLuz(void);
void SendData(void);
void InitWaitingLed(void);
//_Bool GetConection(void);
_Bool GetConection_WEBServ(void);
void Error_Handler(void);
int webservcallback(res* r);
HAL_StatusTypeDef Mem_Write(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size, uint32_t Timeout);
//void RstBuffer(void);


/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

  /* Configure the system clock */
	SystemClock_Config();

  /* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_ADC1_Init();
	MX_I2C1_Init();
	MX_IWDG_Init();
	MX_USART1_UART_Init();

	//HAL_DBGMCU_EnableDBGSleepMode();  //Permite debuguear
	//HAL_DBGMCU_EnableDBGStopMode();

		//GetTime();
		//TimeCtrl = HAL_GetTick();
		InitWifi();
		//ledInicio();

	/* USER CODE BEGIN 2 */
	check();
	Reset();																//1
	if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_13) == GPIO_PIN_RESET) {
		GetConection_WEBServ();
	}else{
		CWMode(1);
		Reset();
	}

	/*memset(i2cbuf, 0, sizeof( i2cbuf));
	Mem_Write(&hi2c1, 0b10100000,  0, 2,  i2cbuf, sizeof(i2cbuf), 1000);					//Limpiamos buffer
	HAL_Delay(250);
	Mem_Write(&hi2c1, 0b10100000,  0, 2, (uint8_t*) "JY-G4S", 6, 1000);						//Guardamos los datos
	HAL_Delay(250);
	Mem_Write(&hi2c1, 0b10100000,  64, 2, (uint8_t*) "12345679", 8, 1000);
	HAL_Delay(250);
	Mem_Write(&hi2c1, 0b10100000,  96, 2, (uint8_t*) device, sizeof(device), 1000);
	HAL_Delay(250);
	Mem_Write(&hi2c1, 0b10100000,  160, 2, (uint8_t*) apikey, sizeof(apikey), 1000);*/

	HAL_Delay(250);
	HAL_I2C_Mem_Read(&hi2c1, 0b10100000, 0,  2, i2cbuf,sizeof(i2cbuf), 1000);

	HAL_Delay(250);
	HAL_I2C_Mem_Read(&hi2c1, 0b10100000, 0,  2, mywlan.ssid,64, 1000);
	HAL_Delay(250);
	HAL_I2C_Mem_Read(&hi2c1, 0b10100000, 64,  2, mywlan.pass,32, 1000);
	HAL_Delay(250);
	HAL_I2C_Mem_Read(&hi2c1, 0b10100000, 96,  2, mywlan.device,64, 1000);
	HAL_Delay(250);
	HAL_I2C_Mem_Read(&hi2c1, 0b10100000, 160,  2, mywlan.apikey,64, 1000);

	//Conect((uint8_t*)"WLAN_2F",7,(uint8_t*)"smallsignals",12);
	Conect(mywlan.ssid,mywlan.ssid_len,
			mywlan.pass,mywlan.pass_len);
	//Conect((uint8_t*)"siemens",7,(uint8_t*)"elaisiemenselai",15);					//3
	//Conect((uint8_t*)"MOVISTAR_CF00",13,(uint8_t*)"B6v99dXvgzjbBWVwX8Us",20);
	//while(GetIp() == 0);																//4
	//SetMux(0);

	//GetTime();
	//sntp_SNTP_GetTime();
	//CloseConnect();

	/* USER CODE END 2 */

	//HAL_IWDG_Start(&hiwdg);

	/* USER CODE BEGIN 3 */
	/* Infinite loop */
 while (1)
  {


	//Prueba I2C--------------------------------------

	 //HAL_I2C_Mem_Write(&hi2c1, 0b10100000,  1, 2, (uint8_t*) "pruemie", 7, 1000) ;

	 /*while(HAL_I2C_Mem_Write(&hi2c1, 0b10100000,  1, 1, (uint8_t*) "prueba", 6, 1000) != HAL_OK){
		 try++;
	 };*/
	 //HAL_Delay(500);
//	 while(HAL_I2C_Mem_Read(&hi2c1, 0b10100000,  1,  2, i2cbuf,7, 1000)!= HAL_OK);
	 //HAL_I2C_Mem_Read(&hi2c1, 0b10100000,  1,  2, i2cbuf,7, 1000);
	 //Prueba I2C--------------------------------------

	// HAL_Delay(5000);




	crashnum++;

		//HAL_ADC_Start_DMA(&hadc1, (uint32_t*)valores, 30);

	//TimeCtrl = HAL_GetTick();    //TEST TIEMPO QUE TARDA EN LLEGAR
	//while((HAL_GetTick())< 41150) HAL_IWDG_Refresh(&hiwdg);
	ledInicio();

		//SendData();

		//SleepWifi();
		//HAL_Delay(250);
		//uint32_t start = HAL_GetTick();
		//waitLed = 0;
		//while((HAL_GetTick()-start)< 600000){			//tiempo que tarda en saltar el iwdg
		//	waitLed++;
		//	InitWaitingLed();
	//HAL_IWDG_Refresh(&hiwdg);
		//};



//	MX_RTC_Init();
//	RTC_AlarmConfig();
//
//	HAL_SuspendTick();//disabilita l'interrupt legato al tick
//	/* Reset SLEEPDEEP bit of Cortex System Control Register */
//	//SCB->SCR &= (uint32_t)~((uint32_t)SCB_SCR_SLEEPDEEP_Msk);		// lo hace la funcion
//	/* Reset SLEEPONEXIT bit of Cortex System Control Register */
//	//SCB->SCR &= (uint32_t)0xFFFFFFFD;
//	HAL_PWR_EnterSTOPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);



	//InitWifi();

	//HAL_IWDG_Refresh(&hiwdg);
	//HAL_IWDG_Start(&hiwdg);
	/*while(1){
		waitLed++;
		InitWaitingLed();
	};*/


		//InitWifi();
		//HAL_Delay(500);
		//Conect((uint8_t*)"WLAN_2F",7,(uint8_t*)"smallsignals",12);

		//while(GetIp() == 0);																//4
	//SetMux(0);

		//GetTime();
		//sntp_SNTP_GetTime();
		//CloseConnect();


  }

 }

// ADC DMA interrupt handler
void HAL_ADC_ConvCpltCallback( ADC_HandleTypeDef * hadc){
	//HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_12 );
	HAL_ADC_Stop_DMA(hadc);
    HAL_ADC_Stop(hadc);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	//HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12,GPIO_PIN_SET );
}
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){
	//HAL_UART_Receive_IT(&huart1,RxBuffer,50);
}


/** System Clock Configuration
*/
void SystemClock_Config(void)
{

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;


	//__PWR_CLK_ENABLE();							//Alarma

	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	  {
	    /* Initialization Error */
	    while(1);
	  }

	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
	  {
	    /* Initialization Error */
	    while(1);
	  }

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_ADC;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
	PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV2;
	HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);

	__HAL_RCC_AFIO_CLK_ENABLE();
	__HAL_RCC_RTC_ENABLE();
}

/* ADC1 init function */
void MX_ADC1_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

    /**Common config*/

  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ENABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 3;
  HAL_ADC_Init(&hadc1);
  hadc1.Instance->SQR1=hadc1.Instance->SQR1|0x200000;
    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_2;
  sConfig.Rank = 2;
  sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
  HAL_ADC_ConfigChannel(&hadc1, &sConfig);

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_8;
  sConfig.Rank = 3;
  sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
  HAL_ADC_ConfigChannel(&hadc1, &sConfig);

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_9;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
  HAL_ADC_ConfigChannel(&hadc1, &sConfig);

}

/* I2C1 init function */
void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 1;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLED;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLED;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLED;
  HAL_I2C_Init(&hi2c1);

}


/* IWDG init function */
void MX_IWDG_Init(void)
{

  hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_128;
  hiwdg.Init.Reload = 3000; 				//(40khz/128)*9.6seconds
  HAL_IWDG_Init(&hiwdg);

}


/* USART1 init function */
void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  HAL_UART_Init(&huart1);

}

/** 
  * Enable DMA controller clock
  */
void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 3);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
     PB6   ------> I2C1_SCL
     PB7   ------> I2C1_SDA
*/
void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __GPIOC_CLK_ENABLE();
  __GPIOD_CLK_ENABLE();
  __GPIOA_CLK_ENABLE();
  __GPIOB_CLK_ENABLE();

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PB2 */
  GPIO_InitStruct.Pin = GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PB10 PB11 PB12 */
  GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PA11 PA12 */
  GPIO_InitStruct.Pin = GPIO_PIN_11|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB6 PB7
  GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);*/

}


void MX_RTC_Init(void)
{

	    /**  Initialize RTC  **/
	  RtcHandle.Instance = RTC;
	  RtcHandle.Init.AsynchPrediv = RTC_AUTO_1_SECOND;
	  RtcHandle.Init.OutPut = RTC_OUTPUTSOURCE_NONE;

	  HAL_RTC_Init(&RtcHandle);
}

static void RTC_AlarmConfig(void)
{

	RTC_DateTypeDef  sdatestructure_set = {0};
	RTC_TimeTypeDef  stimestructure = {0};
	RTC_AlarmTypeDef salarmstructure = {{0}, 0};

  /*##-1- Configure the Date #################################################*/
  /* Set Date: October 31th 2014 */
  sdatestructure_set.Year = 0x14;
  sdatestructure_set.Month = RTC_MONTH_OCTOBER;
  sdatestructure_set.Date = 0x31;

  if(HAL_RTC_SetDate(&RtcHandle,&sdatestructure_set,RTC_FORMAT_BCD) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  /*##-2- Configure the Time #################################################*/
  /* Set Time: 23:59:55 */
  stimestructure.Hours = 0x23;
  stimestructure.Minutes = 0x59;
  stimestructure.Seconds = 0x55;
  if(HAL_RTC_SetTime(&RtcHandle,&stimestructure,RTC_FORMAT_BCD) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  /*##-3- Configure the RTC Alarm peripheral #################################*/
  /* Set Alarm to 00:00:10
     RTC Alarm Generation: Alarm on Hours, Minutes and Seconds */
  salarmstructure.Alarm = RTC_ALARM_A;
  salarmstructure.AlarmTime.Hours = 0x00;
  salarmstructure.AlarmTime.Minutes = 0x08;
  salarmstructure.AlarmTime.Seconds = 0x33;

  if(HAL_RTC_SetAlarm_IT(&RtcHandle,&salarmstructure,RTC_FORMAT_BCD) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

}



void Error_Handler(void)
{
  while(1)
  {

  }
}





void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/*
int FindRep(uint8_t *RxBuffer,int lenRx,uint8_t *Reply,int lenRep){

	for(int i=0; i<lenRx;i++){
		if(strncmp(RxBuffer+i,Reply,lenRep)==0){
//		for(int x=0;x<lenRep-1;x++){
//
//			if(RxBuffer[i+x]==Reply[x]){
//				if(x==(lenRep-2)){
					exito[cexito]=1;
					cexito++;
					return i;
//				return i;
//				}
//			}else break;
		}
	}
	exito[cexito]=0;
	cexito++;
return -1;
}



_Bool SendComand(uint8_t TxBuffer[],int lenTx,int lenRx){


			HAL_UART_Receive_IT(&huart1,RxBuffer,lenRx);
			if(HAL_UART_GetState(&huart1) != HAL_UART_STATE_RESET )
					{
						HAL_UART_Receive_IT(&huart1,RxBuffer,lenRx);
						if(HAL_UART_Transmit_IT(&huart1, TxBuffer, lenTx) == HAL_OK){
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_SET );
					}

			HAL_Delay(1800);
			return 1;
}
return 0;
}



void RstBuffer(void){
	for(int i=0; i<1024; i++){
		RxBuffer[i]=0;
	}
}


_Bool check(){
	uint8_t Reply[2]="OK";
	uint8_t TxBuffer[] = "AT\r\n";
		for(int e = 0; e<3; e++){
			_Bool Enviar = SendComand(TxBuffer, 4,11);
			if (1==Enviar){
				int encontrado = FindRep(RxBuffer, 11, Reply, sizeof(Reply));
				if(encontrado != -1){
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12,GPIO_PIN_SET );
					RstBuffer();
					return 1;
				}
			}
		}
			RstBuffer();
			return 0;
}

_Bool Reset(){
	uint8_t Reply[]="ready";
	uint8_t TxBuffer[] = "AT+RST\r\n";

		for(int e = 0; e<3; e++){
			_Bool Enviar = SendComand(TxBuffer, 8,294);
			if (1==Enviar){
				int encontrado = FindRep(RxBuffer, 294, Reply, sizeof(Reply)-1);
				if(encontrado != -1){
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12,GPIO_PIN_SET );
					RstBuffer();
					return 1;
				}
			}
		}
			RstBuffer();
			return 0;
}

*/


int GetTemp(void){

	int ttemp=0;
	for(int d=0; d<30; d+=3){
		ttemp+=valores[d];
	}
	//int valor =ttemp/10;
	//int vout = (valor*3300/4096);
	//int Temperatura = (vout - 500) / 10;

	int vout = (ttemp*3300/40960);
	int Temperatura = (vout - 500);

	return Temperatura;
	//return vout;

}



int GetConduc(void){

	int ctemp=0;
		for(int d=2; d<30; d+=3){
			ctemp+=valores[d];
		}
		int valor =ctemp/10;

		int vout = (valor*3300/4096);

		return vout;
}

int GetLuz(void){


	int ltemp=0;
	for(int d=1; d<30; d+=3){
		ltemp+=valores[d];
	}
	int valor =ltemp/10;

	int vout = (valor*3300/4096);

	return vout;
}


void SendData(){


	//Reset();
	//RstBuffer();
	SetConnect("82.223.244.60");

	char jon[160];
	char msg[420];
	memset(jon, 0, sizeof(jon)); //------------->
	memset(msg, 0, sizeof(msg)); //------------->
	SNTP_Timestamp_t ntpTime;
	sntp_NTPGetTime(&ntpTime, 0);
	int lenj =e_sprintf(jon,"{\"protocol\":\"v2\",\"device\":\"%s\",\"at\":%l,\"data\":{\"LighT\":\"%04d\" , \"conduct\":\"%04d\" , \"Temperature\":\"%03d.%d\"}}\n"
	//,(ntpTime.seconds-(uint32_t)2208988800), crashnum,GetConduc(),GetTemp()/10,GetTemp()%10);
	  ,device,(ntpTime.seconds-(uint32_t)2208988800), GetLuz(),GetConduc(),GetTemp()/10,GetTemp()%10);
	//,(ntpTime.seconds-(uint32_t)2208988800), GetLuz(),GetConduc(),GetTemp()); ,GetTemp()-(GetTemp()/10)*10);
	int lenn = e_sprintf(msg,
			"POST /streams HTTP/1.1\n"
			"Host: api.carriots.com\n"
			"Accept: application/json\n"
			"User-Agent: Arduino-Carriots\n"
			"Content-Type: application/json\n"
			"carriots.apikey: %s\n"
			"Content-Length: %d\n"
			"Connection: close\n"
			"\n"
			"%s"
			,apikey,lenj,jon);

//	SendComand((uint8_t *) "AT+CIPSTATUS\r\n", 14, 61,TimeStatus);

			while(0 == SendToServer((uint8_t*) msg, lenn));

//	SendComand((uint8_t *) "AT+CIPSTATUS\r\n", 14, 61,TimeStatus);
			CloseConnect();

}



void ledInicio(void){
	 		/*HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_SET);
	  		HAL_Delay( 200 );
	  		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_RESET);
	  		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_SET);
	  		HAL_Delay( 200 );
	  		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_RESET);
	  		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12,GPIO_PIN_SET);
	  		HAL_Delay( 200 );
	  		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12,GPIO_PIN_RESET);
	  		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_SET);
	  		HAL_Delay( 300 );
	  		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_RESET);
	  		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_SET);
	  		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_SET);
	  		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12,GPIO_PIN_SET);
	  		HAL_Delay( 600 );
	  		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_RESET);
	  		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_RESET);
	  		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12,GPIO_PIN_RESET);
	  		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_SET);
	  		HAL_Delay( 10 );
	  		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_RESET);*/
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_SET);
			HAL_Delay( 200 );
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12,GPIO_PIN_SET);
			HAL_Delay( 200 );
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12,GPIO_PIN_RESET);
			HAL_Delay( 200 );
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_RESET);
			HAL_Delay( 200 );


}


void InitWaitingLed(void){

			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12,GPIO_PIN_RESET);

	if (waitLed <97500){

	  		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_RESET);

	}else if(waitLed <100000){

			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_SET);

	}else {

		waitLed = 0;

	}

}

_Bool GetConection_Manual(){

		ledInicio();
		CWMode(1);
		Reset();
		DesConect();

		Conect((uint8_t*)"JY-G4S",6,(uint8_t*)"12345679",8);
		if(GetIp() != 0){
			SetMux(0);
			ledInicio();
			return 1;
		};

		DesConect();

		Conect((uint8_t*)"MOVISTAR_7AA7",13,(uint8_t*)"B6v99dXvgzjbBWVwX8Us",20);
		if(GetIp() != 0){
			SetMux(0);
			ledInicio();
			return 1;
		};

		DesConect();

		Conect((uint8_t*)"siemens",7,(uint8_t*)"elaisiemenselai",15);
		if(GetIp() != 0){
			SetMux(0);
			ledInicio();
			return 1;
		};

		DesConect();

		Conect((uint8_t*)"WLAN_2F",7,(uint8_t*)"smallsignals",12);
		if(GetIp() != 0){
			SetMux(0);
			ledInicio();
			return 1;
		};

		SetMux(0);
		return 0;

}
_Bool GetConection_WEBServ(){
		char txBuffer[500];
		ledInicio();
		CWMode(2);
		Reset();
		SetMux(1);
		SetServer(1,80);
		GetIp();
//		SetMux(0);

		HAL_UART_Receive_IT(&huart1,RxBuffer,1000);
		uint16_t oldcount;
		uint8_t state = 0; //0 wait for ipd, 1 wait for :, 3 wait for complete the count.
		int l;
		int datasize = 0;
		int comienzo;
		int ipd_pos=-1;
		int id =0;
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12,GPIO_PIN_SET);

		while (1){
			if (huart1.RxXferCount == 0||huart1.State == HAL_UART_STATE_READY){
				HAL_UART_Receive_IT(&huart1,RxBuffer,1000);
				state=0;
			}
			if (huart1.RxXferCount != oldcount){
				switch (state){
					case 0://wait for ipd
						l=FindRep((uint8_t*)RxBuffer,1000,(uint8_t*)"+IPD",4);
						if (l >-1){
							state = 1;
							HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_RESET);
							oldcount =huart1.RxXferSize-(l+4);
							ipd_pos=l;
						}else{
							oldcount = huart1.RxXferCount;
						}
						break;
					case 1: //wait for :
						l=FindRep((uint8_t*)&(RxBuffer[ipd_pos+4]),1000,(uint8_t*)":",1);
						if (l >-1){
							comienzo = l + ipd_pos+4;
							state = 2;
							HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_RESET);
							RxBuffer[comienzo]=0; // this removes the : in favour of a null terminated sring
							comienzo++;
							RxBuffer[ipd_pos+6]=0; // this removes the , in favour of a null terminated sring
							datasize = myAtoi((char*)&(RxBuffer[ipd_pos+7]));
							id=myAtoi((char*)&(RxBuffer[ipd_pos+5]));
						}
						oldcount = huart1.RxXferCount;
						break;
					case 2:
						if (huart1.RxXferSize-huart1.RxXferCount-comienzo >= datasize){
							HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_RESET);
							state = 0;
							//stopping uart receive
						      __HAL_UART_DISABLE_IT(&huart1, UART_IT_RXNE);

						      /* Check if a transmit process is ongoing or not */
						      if(huart1.State == HAL_UART_STATE_BUSY_TX_RX)
						      {
						    	  huart1.State = HAL_UART_STATE_BUSY_TX;
						      }
						      else
						      {
						        /* Disable the UART Parity Error Interrupt */
						        __HAL_UART_DISABLE_IT(&huart1, UART_IT_PE);

						        /* Disable the UART Error Interrupt: (Frame error, noise error, overrun error) */
						        __HAL_UART_DISABLE_IT(&huart1, UART_IT_ERR);

						        huart1.State = HAL_UART_STATE_READY;
						      }

							huart1.RxXferCount=0;
							//end stopping uart receive
							RxBuffer[comienzo+datasize]=0;

							resource_list[0].callback=webservcallback;
							strcpy(resource_list[0].param[0],"ssid"); resource_list[0].param_length[0]=4;
							strcpy(resource_list[0].param[1],"pass"); resource_list[0].param_length[1]=4;
							resource_list[0].numparam=2;
							strcpy(resource_list[0].endpoint,"/");
							resource_list[0].endpoint_length=1;

							Service_Request((char*)&RxBuffer[comienzo],txBuffer);
							SendToServerMult(id,(uint8_t*)txBuffer,strlen(txBuffer));

							HAL_Delay(2000);

							webservcallback(&(resource_list[0]));

							return 1;
						}
						break;
					default:
						break;
				}
			}

		}
		return 0;

}


int webservcallback(res* r){
	ledInicio();
		CWMode(1);
		Reset();
		DesConect();

		//Conect((uint8_t*) r->parm_values[0],strlen(r->parm_values[0]),
		//	   (uint8_t*) r->parm_values[1],strlen(r->parm_values[1]));

		Mem_Write(&hi2c1, 0b10100000,  96, 2, (uint8_t*) r->parm_values[0], strlen(r->parm_values[0]), 1000);
		HAL_Delay(250);
		Mem_Write(&hi2c1, 0b10100000,  96, 2, (uint8_t*) r->parm_values[1], strlen(r->parm_values[1]), 1000);
		HAL_Delay(250);
		Mem_Write(&hi2c1, 0b10100000,  96, 2, (uint8_t*) r->parm_values[2], strlen(r->parm_values[2]), 1000);
		HAL_Delay(250);
		Mem_Write(&hi2c1, 0b10100000,  96, 2, (uint8_t*) r->parm_values[3], strlen(r->parm_values[3]), 1000);
		HAL_Delay(250);



		if(GetIp() != 0){
			SetMux(0);
			ledInicio();
			return 1;
		}else{
			return 0;
		}
}


int GetConection_Eeprom(){
		ledInicio();
		CWMode(1);
		Reset();
		DesConect();

		Conect(mywlan.ssid,mywlan.ssid_len,
			    mywlan.pass,mywlan.pass_len);
		if(GetIp() != 0){
			SetMux(0);
			ledInicio();
			return 1;
		}else{
			return 0;
		}
}


HAL_StatusTypeDef Mem_Write(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size, uint32_t Timeout){

	HAL_StatusTypeDef sStatus;
	int scount;
	scount = Size / 32;

	if(Size < 32){
		return HAL_I2C_Mem_Write(hi2c, DevAddress,  MemAddress, MemAddSize,  pData, Size, Timeout);
	}else{
		while (scount >= 0 ){

			if(scount == 0){
				scount =- 1;
				return HAL_I2C_Mem_Write(hi2c, DevAddress,  MemAddress, MemAddSize,  pData, Size%32, Timeout);
			}else{
				sStatus = HAL_I2C_Mem_Write(hi2c, DevAddress,  MemAddress, MemAddSize,  pData, 32, Timeout);
				HAL_Delay(250);
				if (HAL_OK != sStatus) return sStatus;
				for(int i=0;i<32;i++){
					pData[i]=pData[i+32];
				}
				MemAddress += 32;
				scount -= 1;
			}
		}
	}
	return sStatus;
}



/**
  * @}
*/ 
void _init(){

}
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
