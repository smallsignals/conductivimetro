/*

  HELPER.C
  ========
  (c) Copyright Paul Griffiths 1999
  Email: mail@paulgriffiths.net

  Implementation of helper functions for simple web server.
  The Readline() and Writeline() functions are shamelessly
  ripped from "UNIX Network Programming" by W Richard Stevens.

*/


#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <ctype.h>

#include "helper.h"


/*  Prints an error message and quits  */

void Error_Quit(char const * msg) {
    fprintf(stderr, "WEBSERV: %s\n", msg);
    exit(EXIT_FAILURE);
}

/*  Read a line from a buffer */
ssize_t Readline(char* buf, void *vptr, size_t maxlen) {
    ssize_t n;
    char   *buffer;

    buffer = vptr;
    for ( n = 0; n < maxlen; n++ ) {
    	//if ( buf[n]!=0 ) {
    		*buffer++ = buf[n];
    		if ( buf[n] == '\n' )
    			break;
    	//}else{
    	//	return 0;
    	//}
    }

    *buffer = 0;
    return n;
}
 



/*  Removes trailing whitespace from a string  */

int Trim(char * buffer) {
    int n = strlen(buffer) - 1;

    while ( !isalnum(buffer[n]) && n >= 0 )
	buffer[n--] = '\0';

    return 0;
}


/*  Converts a string to upper-case  */
    
int StrUpper(char * buffer) {
    while ( *buffer ) {
	*buffer = toupper(*buffer);
	++buffer;
    }
    return 0;
}


/*  Cleans up url-encoded string  */

void CleanURL(char * buffer) {
	char asciinum[3] = {0};
	int i = 0, c;

	while ( buffer[i] ) {
		if ( buffer[i] == '+' )
			buffer[i] = ' ';
		else if ( buffer[i] == '%' ) {
			asciinum[0] = buffer[i+1];
			asciinum[1] = buffer[i+2];
			buffer[i] = strtol(asciinum, NULL, 16);
			c = i+1;
			do {
				buffer[c] = buffer[c+2];
			} while ( buffer[2+(c++)] );
		}
		++i;
	}
}

int strncasecmp(const char *s1, const char *s2, size_t len) {
  int diff = 0;

  if (len > 0)
    do {
      diff = tolower(*s1) - tolower(*s2);
      s1++;
      s2++;
    } while (diff == 0 && s1[-1] != '\0' && --len > 0);

  return diff;
}

