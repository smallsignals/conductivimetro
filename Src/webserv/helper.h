/*

  HELPER.H
  ========
  (c) Copyright Paul Griffiths 1999
  Email: paulgriffiths@cwcom.net

  Interface to helper functions for simple webserver.

*/


#ifndef PG_HELPER_H
#define PG_HELPER_H

#include <string.h>
#include <unistd.h>          /*  for ssize_t data type  */
#include "printf.h"

/*  Function prototypes  */

void    Error_Quit(char const * msg);
int     Trim      (char * buffer);
int     StrUpper  (char * buffer);
void    CleanURL  (char * buffer);
ssize_t Readline(char* buf, void *vptr, size_t maxlen) ;
int strncasecmp(const char *s1, const char *s2, size_t len) ;

/*  Global macros/variables  */

#define LISTENQ          (1024)


#endif  /*  PG_HELPER_H  */
