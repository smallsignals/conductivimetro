/*

  RESOURCE.C
  ==========
  (c) Copyright Paul Griffiths 1999
  Email: mail@paulgriffiths.net

  Implementation of functions for returning a resource.

*/


#include <unistd.h>
#include <fcntl.h>

#include <string.h>
#include <stdio.h>

#include "resource.h"
#include "reqhead.h"
#include "helper.h"


res resource_list[MAX_RESNUMBER];
int resource_num = MAX_RESNUMBER;

/*  Returns a resource  */

int Return_Resource(char *buff, int resource, struct ReqInfo * reqinfo) {

	e_sprintf (buff,"%sOK",buff);
    return 0;
}
/**
 * parases var value chains looking for a var returning val. var ara cas unsemsitive
 * @param data data is "var1=val1&var2=val2...". Find variable first
 * @param data_len length of data
 * @param name name to find
 * @param dst resultant value for that name
 * @param dst_len
 * @param n in case of several variables with the same name which on returns. start on 0
 * @return -2 -> destination buffer too small
 */
static int get_var(const char *data, size_t data_len, const char *name,
                   char *dst, size_t dst_len, int n) {
  const char *p, *e = data + data_len, *s;
  size_t name_len;
  int i = 0, len = -1;

  if (dst == NULL || dst_len == 0) {
    len = -2;
  } else if (data == NULL || name == NULL || data_len == 0) {
    dst[0] = '\0';
  } else {
    name_len = strlen(name);
    dst[0] = '\0';

    // data is "var1=val1&var2=val2...". Find variable first
    for (p = data; p + name_len < e; p++) {
      if ((p == data || p[-1] == '&' || p[-1] == '?') && p[name_len] == '=' &&
          !strncasecmp(name, p, name_len)) {

        if (n != i++) continue;

        // Point p to variable value
        p += name_len + 1;

        // Point s to the end of the value
        s = (const char *) memchr(p, '&', (size_t)(e - p));
        if (s == NULL) {
          s = e;
        }
        if (!(s >= p)) return -1;

        // Decode variable into destination buffer
        //len = mg_url_decode(p, (size_t)(s - p), dst, dst_len, 1);
        strncpy(dst,p,(size_t)(s - p));
        dst[(size_t)(s - p)]=0;
        // Redirect error code from -1 to -2 (destination buffer too small).
        if (len == -1) {
          len = -2;
        }
        break;
      }
    }
  }

  return len;
}

/*  Tries to open a resource. The calling function can use
    the return value to check for success, and then examine
    errno to determine the cause of failure if neceesary.    */

int Check_Resource(struct ReqInfo * reqinfo) {

    /*  Resource name can contain urlencoded
	data, so clean it up just in case.    */

    CleanURL(reqinfo->resource);

    
    /*  loop through resource names, and try to open  */
    for (int i = 0; i < resource_num; i++){
        if (strncmp(resource_list[i].endpoint,reqinfo->resource,resource_list[i].endpoint_length)==0){
        	for (int j = 0; j < resource_list[i].numparam;j++){
        		get_var(reqinfo->resource,strlen(reqinfo->resource),
        				resource_list[i].param[j],resource_list[i].parm_values[j],20,0);
        	}
        	//callback
        	//resource_list[i].callback(&(resource_list[i]));
        	return 1;
        }

    }
    return -1;
    //strcat(server_root, reqinfo->resource);
    //return open(server_root, O_RDONLY);
}


/*  Returns an error message  */
int Return_Error_Msg(char* buffer, struct ReqInfo * reqinfo) {
    
    //char buffer[100];

    e_sprintf(buffer, "<HTML>\n<HEAD>\n<TITLE>Server Error %d</TITLE>\n"
	            	"</HEAD>\n\n <BODY>\n<H1>Server Error %d</H1>\n"
    				"<P>The request could not be completed. Try again</P>\n"
    			    "</BODY>\n</HTML>\n", reqinfo->status,reqinfo->status);

    return 0;

}
