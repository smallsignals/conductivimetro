/*

  RESOURCE.H
  ==========
  (c) Copyright Paul Griffiths 1999
  Email: paulgriffiths@cwcom.net

  Interface to functions for returning a resource.

*/


#ifndef PG_RESOURCE_H
#define PG_RESOURCE_H


#include "reqhead.h"         /*  for struct ReqInfo  */
#define MAX_RESNUMBER 1
#define PARAM_NUMBER 4
typedef struct res res;
struct res{
	char endpoint[10]; /// end point name
	int endpoint_length; /// lenght of the endpoint string
	char param[PARAM_NUMBER][10]; /// list of param names to be searched in request
	int param_length[PARAM_NUMBER]; /// length for each string of the declared params
	char parm_values[PARAM_NUMBER][64]; /// values for this params.
	int numparam; /// number of parameter for this endpoint
	int (*callback)(res* r); ///callback function that is run when a request is received.
};

extern res resource_list[MAX_RESNUMBER];
extern int resource_num ;
/*  Function prototypes  */
int Return_Resource (char *buf, int resource, struct ReqInfo * reqinfo);
int Check_Resource  (struct ReqInfo * reqinfo);
int Return_Error_Msg(char *buf, struct ReqInfo * reqinfo);


#endif  /*  PG_RESOURCE_H  */
