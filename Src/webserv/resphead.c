/*

  RESPHEAD.C
  ==========
  (c) Copyright Paul Griffiths 1999
  Email: mail@paulgriffiths.net

  Implementation of HTTP reponse header functions.

*/


#include <unistd.h>

#include <stdio.h>

#include "resphead.h"
#include "helper.h"


/*  Outputs HTTP response headers  */

int Output_HTTP_Headers(char* buffer, struct ReqInfo * reqinfo) {

    //char buffer[100];

	e_sprintf(buffer, "HTTP/1.0 %d OK\r\n"
					"Server: PGWebServ v0.1\r\n"
					"Content-Type: text/html\r\n"
					"\r\n", reqinfo->status);

    return 0;
}



