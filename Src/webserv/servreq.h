/*

  SERVREQ.H
  =========
  (c) Copyright Paul Griffiths 1999
  Email: paulgriffiths@cwcom.net

  Interface to function to server connections.

*/


#ifndef PG_SERVREQ_H
#define PG_SERVREQ_H


/*  Function prototypes  */

int Service_Request(char *buff, char* outbuff);


#endif  /*  PG_SERVREQ_H  */










