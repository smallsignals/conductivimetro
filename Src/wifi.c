#include "..\Inc\wifi.h"
#include <string.h>
#include "..\Inc\printf.h"
#include "../Inc/sntp.h"

uint32_t TimeStart = 0;	//perdir a servidor //para ntc sobre 3639060000

uint32_t TickStrt = 0;

extern uint8_t RxBuffer[1024];
extern UART_HandleTypeDef huart1;

extern int exito[8];
extern int cexito;

#define WIFI_RST_PORT GPIOA
#define WIFI_RST_PIN GPIO_PIN_11
#define WIFI_CHPD_PORT GPIOA
#define WIFI_CHPD_PIN GPIO_PIN_12

uint8_t packetBuffer[ NTP_PACKET_SIZE ];   //buffer to hold incoming & outgoing packets
//uint8_t packetBuffer[48];
/**
 * rst 1 chpd 1
 */
void InitWifi(void){
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11,GPIO_PIN_SET); 	/*rst del WIFI*/
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12,GPIO_PIN_SET); 	/*chpd del WIFI*/
}
/**
 * rst 0 chp 0
 */
void DeInitWifi(void){
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11,GPIO_PIN_RESET); 	/*rst del WIFI*/
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12,GPIO_PIN_RESET); 	/*chpd del WIFI*/
}
/**
 * rst 1 chpd 0
 * OFF: CHIP_PD pin is low. The RTC is disabled. All registers are cleared.
 */
void SleepWifi(void){
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11,GPIO_PIN_SET); 	/*rst del WIFI*/
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12,GPIO_PIN_RESET); 	/*chpd del WIFI*/
}
/**
 * rst 0 chpd 1
 */
void RestartWifi(void){
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11,GPIO_PIN_RESET); 	/*rst del WIFI*/
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12,GPIO_PIN_SET); 	/*chpd del WIFI*/
}

/**
 * Busca reply en RxBuffer si no la en cuentra incrementa el cexito
 * @param RxBuffer
 * @param lenRx
 * @param Reply
 * @param lenRep
 * @return -1 si no lo enecuentra y el caracter si lo encuentra.
 */
int FindRep(uint8_t *RxBuffer,int lenRx,uint8_t *Reply,int lenRep){

	for(int i=0; i<lenRx;i++){
		if(strncmp((char*)(RxBuffer+i),(char*)Reply,lenRep)==0){
					return i;
		}
	}
	//exito[cexito]=0;
	//cexito++;
	return -1;
}

/**
 * Send Command to wifi module
 * @param TxBuffer Buffer to transmit
 * @param lenTx Lengh of the buffer to transmit
 * @param lenRx length of the expected reception buffer
 * @param timeRec delay in miliseconds (minimum 50)
 * @return 1 if uart not busy 0 if uart busy
 */
_Bool SendComand(uint8_t TxBuffer[],int lenTx,int lenRx, int timeRec){

	uint32_t start =HAL_GetTick();
	_Bool ret=0;
	while(HAL_UART_GetState(&huart1) == HAL_UART_STATE_RESET  ){//hal can be busy better wait before starting
		if (timeRec < HAL_GetTick()-start) return 0 ;
	}
	if (HAL_UART_Receive_IT(&huart1,RxBuffer,lenRx+20)== HAL_OK){
		//HAL_UART_Receive_IT(&huart1,RxBuffer,lenRx);
		if(HAL_UART_Transmit_IT(&huart1, TxBuffer, lenTx) == HAL_OK){
			//HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_12 );
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12,GPIO_PIN_SET );
			HAL_Delay(50);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12,GPIO_PIN_RESET );
		}else{
			return 0;
		}
		while (timeRec > HAL_GetTick()-start){
			if (huart1.RxXferSize-huart1.RxXferCount>=lenRx){
				ret = 1;
				break;
			}
		}
		ret = 1;//FIXME NO ES LO MISMO RECIBIR QUE NO RECIBIR
		HAL_UART_Receive_IT_STOP(&huart1);
		//HAL_Delay(timeRec-50);

		return ret;
	}else{
		return 0;
	}
}

/**
 * Empty reception buffer
 */
void RstBuffer(void){
	memset(RxBuffer, 0, sizeof(RxBuffer));
}

/**
 * sends AT expects OK
 * @return 1 if works 0 if fails
 */
_Bool check(){
	uint8_t Reply[2]="OK";
	uint8_t TxBuffer[] = "AT\r\n";
		for(int e = 0; e<3; e++){
			_Bool Enviar = SendComand(TxBuffer, 4,11,TimeCheck);
			if (1==Enviar){
				int encontrado = FindRep(RxBuffer, 11, Reply, sizeof(Reply));
				if(encontrado != -1){

					RstBuffer();
					return 1;
				}
			//cexito--;
			}
		}
			//exito[cexito]=0;
			//cexito++;

			RstBuffer();
			return 0;
}
/**
 * AT+RST
 * @return
 */
_Bool Reset(){
	uint8_t Reply[]="ready";
	uint8_t TxBuffer[] = "AT+RST\r\n";

		for(int e = 0; e<3; e++){
			_Bool Enviar = SendComand(TxBuffer, 8,294,TimeReset);
			if (1==Enviar){
				int encontrado = FindRep(RxBuffer, 294, Reply, sizeof(Reply)-1);
				if(encontrado != -1){

					RstBuffer();
					return 1;
				}

			}
		}
			RstBuffer();
			return 0;
}
/**
 *
 * @param mode 1= Sta, 2= AP, 3=both, Sta is the default mode of router, AP is a normal mode for devices
 * @return
 */
_Bool CWMode(int mode){

	_Bool Enviar = 0;
	uint8_t Reply[2]="OK";
	uint8_t Reply2[6]="no cha";

	for(int e = 0; e<3; e++){
	if (mode == 1){Enviar = SendComand((uint8_t *) "AT+CWMODE=1\r\n",13, 20,TimeMode);}
	if (mode == 2){Enviar = SendComand((uint8_t *) "AT+CWMODE=2\r\n",13, 20,TimeMode);}
	if (mode == 3){Enviar = SendComand((uint8_t *) "AT+CWMODE=3\r\n",13, 20,TimeMode);}
	if (mode != 1 && mode != 2 && mode !=3) return 0;
	if (1 == Enviar){
			int encontrado = FindRep(RxBuffer, 25, Reply, sizeof(Reply));
			if(encontrado != -1){

				RstBuffer();
				return 1;
			}else{

			encontrado = FindRep(RxBuffer, 25, Reply2, sizeof(Reply2));
			if(encontrado != -1){

				RstBuffer();
				return 1;
			}
			}

		}
	}

return 0;
}


/**
 * AT+GMR check firmware version basic
 * @return
 */
_Bool GetFrm (void){
	_Bool Enviar = 0;
	uint8_t Reply[2]="OK";
	uint8_t TxBuffer[] = "AT+GMR\r\n";

	for(int e = 0; e<3; e++){
		Enviar = SendComand(TxBuffer, 8, 25,TimeFRM);
		if (1==Enviar){
			int encontrado = FindRep(RxBuffer, 25, Reply, sizeof(Reply));
			if(encontrado != -1){
				RstBuffer();
				return 1;
			}

		}
	}

	return 0;
}
/**
 * conneco to access point
 * @param access_point_name
 * @param lenNam
 * @param password
 * @param lenPas
 * @return
 */
_Bool Conect(uint8_t *access_point_name,int lenNam, uint8_t *password, int lenPas){

	char msg[23+lenPas+lenNam];
	int lenj =e_sprintf(msg,"AT+CWJAP=\"%s\",\"%s\"\r\n",access_point_name,password,TimeConect);


	_Bool Enviar = 0;


uint8_t Reply[2]="OK";
	Enviar = SendComand((uint8_t*)msg, lenj, 23+lenPas+lenNam,TimeConect);
		if (1==Enviar){
			int encontrado = FindRep(RxBuffer, 23+lenPas+lenNam, Reply, sizeof(Reply));
						if(encontrado != -1){
							RstBuffer();
							return 1;

	}
}
	return 0;
}

/**
 * get Ip
 *
 * @return
 */

_Bool GetIp (void){
	_Bool Enviar = 0;
	uint8_t Reply[5]="ERROR";
	uint8_t TxBuffer[] = "AT+CIFSR\r\n";

	for(int e = 0; e<3; e++){

		Enviar = SendComand(TxBuffer, 10, 40,TimeIp+100);
		if (1==Enviar){
			int encontrado = FindRep(RxBuffer, 20,Reply, sizeof(Reply));
			if(encontrado == -1){
				RstBuffer();
				return 1;
			}else {
				RstBuffer();
				return 0;
			};

		}

	}
	RstBuffer();

	return 0;
}


_Bool DesConect(void){

_Bool Enviar = 0;
uint8_t TxBuffer[] = "AT+CWQAP\r\n";
uint8_t Reply[2]="OK";


			Enviar = SendComand(TxBuffer, 10, 17,TimeDconect);
		if (1==Enviar){
			int encontrado = FindRep(RxBuffer, 17, Reply, sizeof(Reply));
			if(encontrado != -1){

				RstBuffer();
				return 1;

	}

}


	return 0;


}



/**
 * CIPSTATUS
 * @return Status: 2 Got Ip, 3 Conected, 4 Disconected.
 */

int GetStatus(void){

	/* Revisar el status devuelto*/

	_Bool Enviar = 0;
	uint8_t TxBuffer[] = "AT+CIPSTATUS\r\n";
	//uint8_t Reply[2]="OK";
	uint8_t Reply2[7]="STATUS:";


	Enviar = SendComand(TxBuffer, 14, 31,TimeStatus);// poner 31 en lugar de 70
	if (1==Enviar){
		int encontrado = FindRep(RxBuffer, 31, Reply2, sizeof(Reply2));
		if(encontrado != -1){
			int status = RxBuffer[encontrado+7];
			status -= 48;
			RstBuffer();
			return status;
		}
		//cexito--;
	}

	//exito[cexito]=0;
	//cexito++;
	return 0;

}

/**
 * TCP/UDP
 *
 * AT+ CIPMUX=0 Single
 * AT+ CIPMUX=1 Multiple
 *
 * @param Mux
 * @return
 */
_Bool SetMux(int Mux){

	_Bool Enviar = 0;
	uint8_t Reply[2]="OK";
	switch ( Mux ) {

	case 0: Enviar = SendComand((uint8_t*)"AT+CIPMUX=0\r\n", 13, 20,TimeMux);
	break;

	case 1: Enviar = SendComand((uint8_t*)"AT+CIPMUX=1\r\n", 13, 20,TimeMux);
	break;

	default: return 0;
	}
	if (1==Enviar){
		int encontrado = FindRep(RxBuffer, 20, Reply, sizeof(Reply));
		if(encontrado != -1){

			RstBuffer();
			return 1;
		}

	}
	return 0;
}


_Bool SetConnect(char* ip){

	_Bool Enviar = 0;
	uint8_t Reply[2]="OK";
	char command[45];
	e_sprintf(command , "AT+CIPSTART=\"TCP\",\"%s\",80\r\n",ip);
	int len = strlen(command);
	Enviar = SendComand((uint8_t*)command,len,55,TimeSetConect);

	//Enviar = SendComand((uint8_t*) "AT+CIPSTART=\"TCP\",\"82.223.244.60\",80\r\n", 40, 55,TimeSetConect);
	//Enviar = SendComand((uint8_t*) "AT+CIPSTART=\"TCP\",\"138.100.101.62\",87\r\n", 41, 55);
	if (1==Enviar){
		int encontrado = FindRep(RxBuffer, 55, Reply, sizeof(Reply));
		if(encontrado != -1){
			RstBuffer();
			return 1;
		}
	}
	return 0;
}
/**
 * Set as server AT+ CIPSERVER= <mode>[,<port>] mode 0 to close server mode; mode 1 to open; port = port
 *
 * @param mode 0 to close server mode; mode 1 to open;
 * @param port
 * @return
 */
_Bool SetServer(uint8_t mode, uint32_t port){
	_Bool Enviar = 0;
	uint8_t Reply[2]="OK";
	char command[43];
	e_sprintf(command , "AT+CIPSERVER=%d,%d\r\n",mode,port);
	int len = strlen(command);
	Enviar = SendComand((uint8_t*)command,len,55,TimeSetConect);
	if (1==Enviar){
		int encontrado = FindRep(RxBuffer, 55, Reply, sizeof(Reply));
		if(encontrado != -1){
			RstBuffer();
			return 1;
		}
	}
	return 0;
}


_Bool CloseConnect(void){

	_Bool Enviar = 0;
	uint8_t Reply[2]="OK";

	int status = 0;
	status = GetStatus();
	if(status != StatusDisconected){

		Enviar = SendComand((uint8_t*) "AT+CIPCLOSE\r\n", 13, 23,TimeSetConect);
		//Enviar = SendComand((uint8_t*) "AT+CIPSTART=\"TCP\",\"138.100.101.62\",87\r\n", 41, 55);
		if (1==Enviar){
			int encontrado = FindRep(RxBuffer, 23, Reply, sizeof(Reply));
			if(encontrado != -1){
				RstBuffer();
				return 1;
			}

		}
	}
	RstBuffer();
	return 0;

}

_Bool SetConnect2(void){

_Bool Enviar = 0;
uint8_t Reply[2]="OK";

Enviar = SendComand((uint8_t*) "AT+CIPSTART=\"UDP\",\"163.117.131.99\",123\r\n", 40, 40+7,TimeSetConect);
//Enviar = SendComand((uint8_t*) "AT+CIPSTART=\"UDP\",\"157.88.129.100\",123\r\n", 40, 40+7,TimeSetConect);
//Enviar = SendComand((uint8_t*) "AT+CIPSTART=\"TCP\",\"138.100.101.62\",87\r\n", 41, 55);
		if (1==Enviar){
			int encontrado = FindRep(RxBuffer, 100, Reply, sizeof(Reply));
			if(encontrado != -1){
				RstBuffer();
				return 1;
	}

}
	RstBuffer();

	return 0;
}
/**
 * send with multiple connection (+CIPMUX=1) AT+CIPSEND= <id>,<length>
 * @param id
 * @param msg
 * @param lenn
 * @return
 */
_Bool SendToServerMult(uint8_t id , uint8_t msg[], int lenn){

	_Bool Enviar = 0;
	uint8_t Reply[2]="OK";
	char msglen[40];
	int len =e_sprintf(msglen,"AT+CIPSEND=%d,%d\r\n",id,lenn);

	Enviar = SendComand((uint8_t*) msglen,len,len+1,TimeSend);
	//RstBuffer();

	Enviar = SendComand(msg ,lenn, lenn+11,1500);   // <------- falla a veces  ------->

	while( Enviar == 0){ledWifiFault();};
	if (1==Enviar){
		int encontrado = FindRep(RxBuffer,lenn+11, Reply, sizeof(Reply));
		if(encontrado != -1){

			RstBuffer();
			return 1;

		}

	}
	RstBuffer();

	return 0;



}

/**
 * Send to server on CIPMUX=0
 * @param msg
 * @param lenn
 * @return
 */
_Bool SendToServer(uint8_t msg[], int lenn){

	_Bool Enviar = 0;
	uint8_t Reply[2]="OK";
	char msglen[40];
	int len =e_sprintf(msglen,"AT+CIPSEND=%d\r\n",lenn);

	Enviar = SendComand((uint8_t*) msglen,len,len+1,TimeSend);
	//RstBuffer();

	Enviar = SendComand(msg ,lenn, lenn+11,1500);   // <------- falla a veces  ------->

	while( Enviar == 0){ledWifiFault();};
	if (1==Enviar){
		int encontrado = FindRep(RxBuffer,lenn+11, Reply, sizeof(Reply));
		if(encontrado != -1){

			RstBuffer();
			return 1;

		}

	}
	RstBuffer();

	return 0;



}


uint32_t GetTime(void)
{
	if(TickStrt==0) TickStrt = HAL_GetTick();
	uint32_t Time = 0;

	Time = TimeStart + ((HAL_GetTick()-TickStrt)/1000);

	return Time;
}




void sendNTPpacket(void)
{
	// set all bytes in the buffer to 0
	memset(packetBuffer, 0, NTP_PACKET_SIZE);
	// Initialize values needed to form NTP request
	// (see URL above for details on the packets)
	packetBuffer[0] = 0b11100011;   // LI, Version, Mode
	packetBuffer[1] = 0;     // Stratum, or type of clock
	packetBuffer[2] = 6;     // Polling Interval
	packetBuffer[3] = 0xEC;  // Peer Clock Precision
	// 8 bytes of zero for Root Delay & Root Dispersion
	packetBuffer[12]  = 49;
	packetBuffer[13]  = 0x4E;
	packetBuffer[14]  = 49;
	packetBuffer[15]  = 52;
	// all NTP fields have been given values, now
	// you can send a packet requesting a timestamp:
	SetConnect2(); //NTP requests are to port 123

	SendComand((uint8_t*) "AT+CIPSEND=48\r\n",15,16,TimeSend);
	RstBuffer();
	SendComand(packetBuffer ,NTP_PACKET_SIZE, NTP_PACKET_SIZE+75,3500);
	//SendComand((uint8_t*) "+IPD,100\r\n",10,100,TimeSend);
	RstBuffer();


}


void ledWifiFault(void){
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_SET);
	HAL_Delay( 200 );
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_SET);
	HAL_Delay( 200 );
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12,GPIO_PIN_SET);
	HAL_Delay( 200 );
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_SET);
	HAL_Delay( 300 );
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12,GPIO_PIN_SET);
	HAL_Delay( 600 );
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_SET);
	HAL_Delay( 10 );
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11,GPIO_PIN_RESET);

}
